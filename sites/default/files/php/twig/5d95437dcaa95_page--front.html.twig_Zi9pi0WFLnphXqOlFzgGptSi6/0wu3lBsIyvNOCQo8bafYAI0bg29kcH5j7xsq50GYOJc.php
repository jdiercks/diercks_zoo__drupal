<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/custom/mytheme/templates/page/page--front.html.twig */
class __TwigTemplate_f81912b1a8fda0d61b6bf7ba0b99574f6fb83052b85e206af873f63db8527052 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["if" => 51];
        $filters = ["escape" => 48];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['if'],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 45
        echo "<div class=\"layout-container\">

  <header role=\"banner\">
    ";
        // line 48
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "header", [])), "html", null, true);
        echo "
  </header>

  ";
        // line 51
        if ($this->getAttribute(($context["page"] ?? null), "mythememenu", [])) {
            // line 52
            echo "    <div id=\"mythememenu\" class=\"content\">
      <div class=\"main-menu\">
        ";
            // line 54
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "mythememenu", [])), "html", null, true);
            echo "
      </div>
    </div>
  ";
        }
        // line 58
        echo "
  <main class=\"front-page_main-content\" role=\"main\">
    <a id=\"main-content\" tabindex=\"-1\"></a>";
        // line 61
        echo "
    <div class=\"layout-content\">
      ";
        // line 63
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "content", [])), "html", null, true);
        echo "
    </div>

    ";
        // line 66
        if ($this->getAttribute(($context["page"] ?? null), "sidebar_first", [])) {
            // line 67
            echo "      <aside class=\"layout-sidebar-first\" role=\"complementary\">
        ";
            // line 68
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "sidebar_first", [])), "html", null, true);
            echo "
      </aside>
    ";
        }
        // line 71
        echo "
  </main>

  ";
        // line 74
        if ($this->getAttribute(($context["page"] ?? null), "services", [])) {
            // line 75
            echo "    <div class=\"services\">
      ";
            // line 76
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "services", [])), "html", null, true);
            echo "
    </div>
  ";
        }
        // line 79
        echo "
  ";
        // line 80
        if ($this->getAttribute(($context["page"] ?? null), "sidebar_footer_left", [])) {
            // line 81
            echo "    <div class=\"zoo_explore zoo_news zoo_weather\">
      ";
            // line 82
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "sidebar_footer_left", [])), "html", null, true);
            echo "
    </div>
  ";
        }
        // line 85
        echo "
  ";
        // line 86
        if ($this->getAttribute(($context["page"] ?? null), "sidebar_footer_right", [])) {
            // line 87
            echo "    <div class=\"events_calendar\">
      ";
            // line 88
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "sidebar_footer_right", [])), "html", null, true);
            echo "
    </div>
  ";
        }
        // line 91
        echo "
  ";
        // line 92
        if ($this->getAttribute(($context["page"] ?? null), "footer", [])) {
            // line 93
            echo "    <footer role=\"contentinfo\">
      ";
            // line 94
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer", [])), "html", null, true);
            echo "
    </footer>
  ";
        }
        // line 97
        echo "
</div>";
    }

    public function getTemplateName()
    {
        return "themes/custom/mytheme/templates/page/page--front.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  162 => 97,  156 => 94,  153 => 93,  151 => 92,  148 => 91,  142 => 88,  139 => 87,  137 => 86,  134 => 85,  128 => 82,  125 => 81,  123 => 80,  120 => 79,  114 => 76,  111 => 75,  109 => 74,  104 => 71,  98 => 68,  95 => 67,  93 => 66,  87 => 63,  83 => 61,  79 => 58,  72 => 54,  68 => 52,  66 => 51,  60 => 48,  55 => 45,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{#
/**
 * @file
 * Theme override to display a single page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.html.twig template in this directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - base_path: The base URL path of the Drupal installation. Will usually be
 *   \"/\" unless you have installed Drupal in a sub-directory.
 * - is_front: A flag indicating if the current page is the front page.
 * - logged_in: A flag indicating if the user is registered and signed in.
 * - is_admin: A flag indicating if the user has permission to access
 *   administration pages.
 *
 * Site identity:
 * - front_page: The URL of the front page. Use this instead of base_path when
 *   linking to the front page. This includes the language domain or prefix.
 *
 * Page content (in order of occurrence in the default page.html.twig):
 * - node: Fully loaded node, if there is an automatically-loaded node
 *   associated with the page and the node ID is the second argument in the
 *   page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - page.header: Items for the header region.
 * - page.primary_menu: Items for the primary menu region.
 * - page.secondary_menu: Items for the secondary menu region.
 * - page.highlighted: Items for the highlighted content region.
 * - page.help: Dynamic help text, mostly for admin pages.
 * - page.content: The main content of the current page.
 * - page.sidebar_first: Items for the first sidebar.
 * - page.sidebar_second: Items for the second sidebar.
 * - page.footer: Items for the footer region.
 * - page.breadcrumb: Items for the breadcrumb region.
 *
 * @see template_preprocess_page()
 * @see html.html.twig
 */
#}
<div class=\"layout-container\">

  <header role=\"banner\">
    {{ page.header }}
  </header>

  {% if page.mythememenu %}
    <div id=\"mythememenu\" class=\"content\">
      <div class=\"main-menu\">
        {{ page.mythememenu }}
      </div>
    </div>
  {% endif %}

  <main class=\"front-page_main-content\" role=\"main\">
    <a id=\"main-content\" tabindex=\"-1\"></a>{# link is in html.html.twig #}

    <div class=\"layout-content\">
      {{ page.content }}
    </div>

    {% if page.sidebar_first %}
      <aside class=\"layout-sidebar-first\" role=\"complementary\">
        {{ page.sidebar_first }}
      </aside>
    {% endif %}

  </main>

  {% if page.services %}
    <div class=\"services\">
      {{ page.services }}
    </div>
  {% endif %}

  {% if page.sidebar_footer_left %}
    <div class=\"zoo_explore zoo_news zoo_weather\">
      {{ page.sidebar_footer_left }}
    </div>
  {% endif %}

  {% if page.sidebar_footer_right %}
    <div class=\"events_calendar\">
      {{ page.sidebar_footer_right }}
    </div>
  {% endif %}

  {% if page.footer %}
    <footer role=\"contentinfo\">
      {{ page.footer }}
    </footer>
  {% endif %}

</div>{# /.layout-container #}
", "themes/custom/mytheme/templates/page/page--front.html.twig", "C:\\NewXampp\\htdocs\\diercksZoo\\themes\\custom\\mytheme\\templates\\page\\page--front.html.twig");
    }
}
